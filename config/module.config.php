<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'EokoBugReporter\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'error' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/error',
                    'defaults' => array(
                        'controller' => 'EokoBugReporter\Controller\Index',
                        'action'     => 'error',
                    ),
                ),
            ),
			
			
            'application' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/bugreporter[/]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'EokoBugReporter\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'simple' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => 'simple[/]',
							'defaults' => array(
								'__NAMESPACE__' => 'EokoBugReporter\Controller',
								'controller'    => 'Index',
								'action'        => 'simple',
							),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'EokoBugReporter\Controller\Index' => 'EokoBugReporter\Controller\IndexController',
        ),
    ),
	
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'error/error'					=> __DIR__ . '/../view/eoko-bug-reporter/error/error.phtml',
            'index/layout'					=> __DIR__ . '/../view/eoko-bug-reporter/full/layout.phtml',
            'index/content'					=> __DIR__ . '/../view/eoko-bug-reporter/index/content.phtml',
            'index/footer'					=> __DIR__ . '/../view/eoko-bug-reporter/full/footer.phtml',
            'simple/layout'					=> __DIR__ . '/../view/eoko-bug-reporter/simple/layout.phtml',
            'simple/navbar'					=> __DIR__ . '/../view/eoko-bug-reporter/simple/navbar.phtml',
            'simple/content'				=> __DIR__ . '/../view/eoko-bug-reporter/index/content.phtml',
            'layout/layout'					=> __DIR__ . '/../view/layout/layout.phtml',
            'EokoBugReporter/index/index'	=> __DIR__ . '/../view/eoko-bug-reporter/index/index.phtml',
            'error/404'						=> __DIR__ . '/../view/error/404.phtml',
            'error/index'					=> __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
