<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace EokoBugReporter\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
	
    public function indexAction()
    {
		$request = $this->getRequest();
		$token = $request->getQuery('token',false);
		
		if(!$request->isGet() || !$token || !$this->isValidToken($token)) {
			$this->redirect()->toRoute('error');
		}
		
		$layoutSimple = new ViewModel();
		$layoutSimple->setTemplate('index/layout');
		$layoutSimple->setTerminal(true);
		
		$content = new ViewModel();
		$content->setTemplate('index/content');
		
		$footer = new ViewModel();
		$footer->setTemplate('index/footer');
		
		$layoutSimple->addChild($content,'content');
		$layoutSimple->addChild($footer,'footer');
		
		return $layoutSimple;
    }
	
	private function isValidToken($token) {
		return true;
	}
	
	public function simpleAction() {		
		$request = $this->getRequest();
		$token = $request->getQuery('token',false);
		
		if(!$request->isGet() || !$token || !$this->isValidToken($token)) {
			$this->redirect()->toRoute('error');
		}
		
		$layoutSimple = new ViewModel();
		$layoutSimple->setTemplate('simple/layout');
		$layoutSimple->setTerminal(true);
		
		
		$navBar = new ViewModel();
		$navBar->setTemplate('simple/navbar');
		
		$content = new ViewModel();
		$content->setTemplate('index/content');
		
		$layoutSimple->addChild($navBar,'navbar');
		$layoutSimple->addChild($content,'content');
		
		return $layoutSimple;
	}
	
	public function errorAction() {
		$errorLayout = new ViewModel();
		$errorLayout->setTemplate('error/error');
		return $errorLayout;
	}
}
